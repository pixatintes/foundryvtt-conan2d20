/***************************************************************
 * This macro can be used to increment/decrement the number of
 * Doom in the Doom pool.
 *
 * @param {number} [value]     The number to adjust the Doom
 *                             pool by.
 *
 * @example
 * game.conan2d20.macros.adjustDoom(-5);
 *
 * @example
 * game.conan2d20.macros.adjustDoom(5);
 **************************************************************/

game.conan2d20.macros.adjustDoom(-1);
