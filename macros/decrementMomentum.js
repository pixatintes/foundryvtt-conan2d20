/***************************************************************
 * This macro can be used to increment/decrement the number of
 * Momentum in the Momentum pool.
 *
 * @param {number} [value]    The number to adjust the Momentum
 *                            pool by.
 *
 * @example
 * game.conan2d20.macros.adjustMomentum(-5);
 *
 * @example
 * game.conan2d20.macros.adjustMomentum(5);
 **************************************************************/

game.conan2d20.macros.adjustMomentum(-1);
